:- module(parsing, [
    get_simple_input/1,
    get_fast_char/1,
    get_fast_atom/1,
    get_fast_term/1,
    write_success/0,
    write_failure/0,
    write_success_message/1,
    write_failure_message/1,
    write_yellow_message/1,
    writeln_fancy/1
]).

get_simple_input(Input_Normalized) :-
    current_input(Input),
    nl,
    ansi_format([bold,fg(yellow)], "Your input: ",[]),   
    read_string(Input, "\n", "\n.", _, String),
    normalize_space(string(Input_Normalized), String).

get_fast_char(Char):-
    get_single_char(Code),
    char_code(Char, Code).

get_fast_atom(Atom):-
    get_single_char(Code),
    char_code(Atom, Code).

get_fast_term(Term):-
    get_fast_atom(Atom),
    atom_to_term(Atom, Term, _).

write_success_message(Message) :-
    ansi_format([bold,fg(green)], "\n~w\n\n",[Message]).
write_failure_message(Message) :-
    ansi_format([bold,fg(red)], "\n~w\n\n",[Message]).
write_yellow_message(Message) :-
    ansi_format([bold,fg(yellow)], "~w",[Message]).

write_success :- write_success_message("Success").
write_failure :- write_failure_message("Failure or Cancelled").

writeln_fancy(Text) :-
    ansi_format([bold,fg(yellow)], Text, []),
    nl.