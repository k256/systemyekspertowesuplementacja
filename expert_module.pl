:- module(expert_module, [
        expert_main/0
    ]).
:- use_module(database).
:- use_module(rules).
:- use_module(parsing).

expert_main :-
    writeln_fancy('Welcome to diabetics expert system'),
    ask_for_action.

write_actions :-
    writeln_fancy('**********************************************'),
    writeln('Please enter the relevant digit to perform an action:'),
    writeln('Kindly note that by typing a single word Cancel you will\n  abort currently performed action and return to home screen.'),
    % writeln('0. Search'),
    write_yellow_message("1. "), writeln("Show all products"),
    write_yellow_message("2. "), writeln("Add new product"),
    write_yellow_message("3. "), writeln("Edit existing product"),
    write_yellow_message("4. "), writeln("Remove product"),
    write_yellow_message("5. "), writeln("Show product characteristics"),
    write_yellow_message("6. "), writeln("Edit product characteristics"),
    write_yellow_message("7. "), writeln("Remove product characteristics"),
    write_yellow_message("8. "), writeln("Quit").

ask_for_action :-
    write_actions,
    get_fast_term(Action),
    ignore(perform_action(Action) -> write_success ; write_failure),
    ask_for_action.

ask_about_property(Prompt, Id, Pair) :-
    write_yellow_message(Prompt),nl,
    get_simple_input(String),
    dif(String, "Cancel"),
    Pair = [Id-String].

ask_about_property(Confirm, Prompt, Id, Pair) :-
    nl,
    write_yellow_message(Confirm),nl,
    write("Please type "),write_yellow_message("y"),write(" to provide data"),nl,
    write("or "),write_yellow_message("n"),write(" to skip this question"),nl,
    get_simple_input(String),
    dif(String, "Cancel"),
    (String = "y" -> ask_about_property(Prompt, Id, Pair); Pair=[]).
    
display_product_property(Key-Value) :-
    % format('    property ~w has value ~w\n', [Key, Value]),
    write("    property "),
    ansi_format([bold,fg(cyan)], "~s~t~40|", [Key]),
    write(" has value "),
    ansi_format([bold,fg(cyan)], "~s~t~40|", [Value]),
    nl.

display_db_entry((Name, Assoc_Values)) :-
    writeln_fancy(Name),
    assoc_to_list(Assoc_Values, List),
    maplist(display_product_property, List).

propose_product(Product) :-
    ansi_format([bold,fg(cyan)], "  - ~w\n", [Product]).

choose_product_bare(Product_Name) :-
    get_simple_input(Input),
    dif(Input, "Cancel"),
    (Input = "Search" -> search_for_product, writeln("Once you're ready type in the name of the product.\nType Search again to perform another search."), choose_product_bare(Product_Name)
    ; (load_assoc_database(Input, _) -> Product_Name = Input ; writeln("Sorry, no such product. Try again.\nType Search to search for a product or Cancel to cancel."), choose_product_bare(Product_Name))).


choose_product(Product_Name) :-
    % writeln("Please enter the name of the product now."),
    writeln("You can always type Search to search for available products"),
    choose_product_bare(Product_Name).

search_for_product(Prefix) :-
    string_chars(Prefix, Chars),
    findall(Name, (load_assoc_database(Name, _), string_chars(Name, Product_Chars), prefix(Chars, Product_Chars)), Bag),
    writeln("Search results:"),
    maplist(propose_product, Bag).

search_for_product :-
    writeln("Please enter a prefix to see all suggestions"),
    get_simple_input(Prefix),
    dif(Prefix, "Cancel"),
    search_for_product(Prefix).

% perform_action(0) :-
%     writeln_fancy('*********Search for product*********'),
%     search_for_product.

perform_action(1) :-
    writeln_fancy('*******Show all products action*******'),
    findall((Name, Assoc_Values), load_assoc_database(Name, Assoc_Values), Bag),
    maplist(display_db_entry, Bag).

perform_action(2) :-
    writeln_fancy('*******Add new product action*******'),
    List0 = [],
    ask_about_property('What is the name of product/dish?', 'dish', [dish-Product_Name]),
    ask_about_property('Would you like to enter calories amount?', 'How many calories does it have? (in grams)', 'calories', Calories),
    append(List0, Calories, List1),
    ask_about_property('Would you like to enter vitamins?', 'What vitamins does it have? (A, B, C)', 'vitamins', Vitamins),
    append(List1, Vitamins, List2),
    ask_about_property('Would you like to enter fat?', 'How much fat does it have? (in grams)', 'fat', Fat),
    append(List2, Fat, List3),
    ask_about_property('Would you like to enter the easiness of digestion?', 'How easy is it for your stomache? (easy, medium, difficult)', 'digestiveness', Digestiveness),
    append(List3, Digestiveness, List4),
    list_to_assoc(List4, Assoc_Values),
    assert_to_database(Product_Name, Assoc_Values).

perform_action(3) :-
    writeln_fancy('*******Edit existing product action*******'),
    writeln('Please enter product name which name you wish to edit:'),
    choose_product(Product_Name),
    load_assoc_database(Product_Name, Assoc_Values),
    writeln('Please enther new product name:'),
    get_simple_input(New_Product_Name),
    dif(New_Product_Name, "Cancel"),
    delete_from_database(Product_Name),
    assert_to_database(New_Product_Name, Assoc_Values).

perform_action(4) :-
    writeln_fancy('*******Remove product action*******'),
    writeln('Please enter product name you wish to remove:'),
    choose_product(Product_Name),
    delete_from_database(Product_Name).

perform_action(5) :-
    writeln_fancy('*******Show product characteristics action*******'),
    writeln('Please enter product name you wish to see:'),
    choose_product(Product_Name),
    load_assoc_database(Product_Name, Assoc_Values),
    display_db_entry((Product_Name, Assoc_Values)).

perform_action(6) :-
    % TODO restrict properties to valid ones
    writeln_fancy('*******Edit product characteristics action*******'),
    writeln('Please enter product name which characteristics you wish to change:'),
    choose_product(Product_Name),
    load_assoc_database(Product_Name, Assoc_Values),
    writeln('Please enter characteristic you wish to change:'),
    get_simple_input(Characteristic),
    dif(Characteristic, "Cancel"),
    atom_string(Atom, Characteristic),
    writeln('Please write new value for chosen characteristic:'),
    get_simple_input(New_Value),
    dif(New_Value, "Cancel"),
    put_assoc(Atom, Assoc_Values, New_Value, New_Assoc_Values),
    delete_from_database(Product_Name),
    assert_to_database(Product_Name, New_Assoc_Values).

perform_action(7) :-
    writeln_fancy('*******Remove product characteristics*******'),
    writeln('Please enter product name which characteristics you wish to remove:'),
    choose_product(Product_Name),
    load_assoc_database(Product_Name, Assoc_Values),
    writeln('Please enter characteristic you wish to remove:'),
    get_simple_input(Characteristic),
    dif(Characteristic, "Cancel"),
    atom_string(Atom_Characteristic, Characteristic),
    del_assoc(Atom_Characteristic, Assoc_Values, _, New_Assoc_Values),
    delete_from_database(Product_Name),
    assert_to_database(Product_Name, New_Assoc_Values).

perform_action(8) :-
    halt.

:- 
    expert_main.
