:- use_module(user_module, [user_main/0]).
:- use_module(expert_module, [expert_main/0]).

read_input :-
    read(CHAR),
    perform_action(CHAR).

menu :-
    writeln("---------Diet expert system---------"),
    writeln("Please select the module:"),
    writeln("1. Expert module"),
    writeln("2. User module"),
    writeln("3. Quit"),
    read_input.

perform_quit :-
    halt.

perform_action(1) :-
    expert_main.

perform_action(2) :-
    user_main.

perform_action(3) :-
    halt.

perform_action(_) :-
    writeln("You misclicked. Try again"),
    read_input.

:- 
    menu.
