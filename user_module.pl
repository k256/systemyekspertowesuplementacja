:- module(user_module, [
        user_main/0
    ]).

:- use_module(database).
:- use_module(library(dcg/basics)).
:- use_module(rules).
:- use_module(parsing).

cyan(Word) :- ansi_format([bold,fg(cyan)], '~w', [Word]).
yellow(Word) :- ansi_format([bold,fg(yellow)], '~w', [Word]).
green(Word) :- ansi_format([bold,fg(green)], '~w', [Word]).
magenta(Word) :- ansi_format([bold,fg(magenta)], '~w', [Word]).

my_string(String) --> {string_chars(String, List)}, List.
number_input(between(Low, High)) --> my_string("between "), integer(Low), my_string(" and "), integer(High), {High >= Low}.
number_input(less_than(Threshold)) --> my_string("less than "), integer(Threshold).
number_input(more_than(Threshold)) --> my_string("more than "), integer(Threshold).

fluentConsecutive([])     --> [].
fluentConsecutive([L|Ls]) --> [L], {char_type(L, alnum) ; char_type(L, white)}, fluentConsecutive(Ls).
fluent([L|Ls]) --> [L], { char_type(L, alnum) }, fluentConsecutive(Ls).
csv_to_list([S]) --> fluent(F),{string_chars(S, F)}.
csv_to_list([S|Others]) --> fluent(F),{string_chars(S, F)},([',']|[',',' ']|[' ',',']|[' ',',',' ']),csv_to_list(Others).

convert_string_csv_into_list_of_values(String, List) :-
    string_chars(String, Chars),
    phrase(csv_to_list(List), Chars).

% :- convert_string_csv_into_list_of_values("A", List), write(List).
% :- convert_string_csv_into_list_of_values("A,B", List), write(List).
% :- convert_string_csv_into_list_of_values("A, a sfB, Csaf", List), write(List).

show_subset_option(Option) :-
    write("Possible option: "),
    yellow(Option),
    nl.

flipped_member(Superset, E) :-
    member(E, Superset).

ask_user_about(number, Question_Text, Question_Key, Answer) :-
    % green(Question_Key),
    once(
        (
            yellow(Question_Text),nl,
            writeln('Please enter a range of integers e.g. '),
            yellow('less than'),write(' 50'),nl,
            yellow('more than'),write(' 50'),nl,
            yellow('between'),write(' 50 '),yellow('and'),write(' 100 '),nl,
            get_simple_input(Input_Normalized),
            string_chars(Input_Normalized, String_Chars),
            phrase(number_input(Answer), String_Chars)
        )
        ;
        (
            write_failure_message("Incorrect input, please try again."),
            ask_user_about(number, Question_Text, Question_Key, Answer)
        )
    ).

ask_user_about(subset, Question_Text, Question_Key, Answer) :-
    once(
        (
            yellow(Question_Text),nl,
            writeln('Please enter values seperated by commas.'),
            findall(Valid_Option, 
                (
                    valid_subset_answer(Question_Key, Atom_Valid_Option),
                    string_to_atom(Valid_Option, Atom_Valid_Option)
                ),
                Valid_Options),
            maplist(show_subset_option, Valid_Options),
            % nl,
            get_simple_input(Input_Normalized),
            convert_string_csv_into_list_of_values(Input_Normalized, Answer),
            maplist(flipped_member(Valid_Options), Answer)
        )
        ;
        (
            write_failure_message("Incorrect input, please try again."),
            ask_user_about(subset, Question_Text, Question_Key, Answer)
        )
    ).

% :- ask_user_about(subset, "hi", fasf, Answer),writeln(Answer).

ask_user_about(fuzzy_subset, Question_Text, Question_Key, Answer) :- 
    once(
        (
            yellow(Question_Text),nl,
            writeln('Please enter values seperated by commas.'),
            findall(Valid_Option,
                (
                    valid_fuzzy_subset_answer(Question_Key, Atom_Valid_Option, _),
                    string_to_atom(Valid_Option, Atom_Valid_Option)
                ),
                Valid_Options),
            maplist(show_subset_option, Valid_Options),
            % nl,
            get_simple_input(Input_Normalized),
            convert_string_csv_into_list_of_values(Input_Normalized, Answer),
            maplist(flipped_member(Valid_Options), Answer)
        )
        ;
        (
            write_failure_message("Incorrect input, please try again."),
            ask_user_about(fuzzy_subset, Question_Text, Question_Key, Answer)
        )
    ).

number_case(less_than(Threshold), Database_Value, Score) :-
    Threshold80 is Threshold -0.05,
    Score is 0.5 + 1/pi * atan((Database_Value - Threshold)/(Threshold80 - Threshold) * tan(pi*(0.8 - 0.5))).
number_case(greater_than(Threshold), Database_Value, Score) :-
    Threshold80 is Threshold +0.05,
    Score is 0.5 + 1/pi * atan((Database_Value - Threshold)/(Threshold80 - Threshold)*tan(pi*(0.8 - 0.5))).
number_case(between(Low, High), Database_Value, Score) :-
    number_case(less_than(High), Database_Value, ScoreLT),
    number_case(greater_than(Low), Database_Value, ScoreGT),
    Midpoint is (High - Low) / 2.0,
    number_case(greater_than(Low), Midpoint, Maximum),
    ScoreLTNormalized is ScoreLT / Maximum,
    ScoreGTNormalized is ScoreGT / Maximum,
    Score is min(ScoreLTNormalized, ScoreGTNormalized).


% subset_score([], _, 0).

% subset_score([A|Answers], Database_Value, Score) :-
%     c(A),nl,
%     (A = Database_Value)
%     -> Score is 1
%     ; subset_score(Answers, Database_Value, Score)).
    


compute_score(number, _, Answer, Database_Value, Score) :-
    number_string(Database_Number, Database_Value),
    number_case(Answer, Database_Number, Score).
compute_score(subset, _, Answer, Database_Value, Score) :-
    member(Database_Value, Answer)
    -> Score is 1
    ; Score is 0.

compute_score(fuzzy_subset, Key, Answer, Database_Value, Score) :-
    valid_fuzzy_subset_answer(Key, Database_Value, B),
    aggregate_all(max(S),
        (
            member(Valid, Answer),
            valid_fuzzy_subset_answer(Key, Valid, A),
            S is 1/(1+abs((A-B)*5)**4)
        ), Score)
    -> true
    ; Score is 1.

compute_optimistic_score(Knowledge, Properties, Score) :-
    aggregate_all(min(S),
        (
            gen_assoc(Key, Properties, Value),
            get_assoc(Key, Knowledge, User_Answer),
            % write("key,user_answer,value"),y(Key),c(User_Answer),m(Value),nl,
            question(Key, _, Question_Category),
            compute_score(Question_Category, Key, User_Answer, Value, S)
            % y(S),nl
        ), Score)
    -> true
    ; Score is 1.
    
% TODO compute_optimistic_score: score is the minimum of optimistic scores over properties (assoc list) when applied valuation wrt to knowledge
% TODO compute_optimistic_score: if there is no property in knowledge, we ignore it, assume optimistic case (question hasn't been asked yet)

ask_question(Key0, Answer) :- 
    question(Key0, Question_Text, Question_Type),
    ask_user_about(Question_Type, Question_Text, Key0, Answer).

display_product_property(_, Key-Value) :-
    % format('    property ~w has value ~w\n', [Key, Value]),
    write("    property "),
    ansi_format([bold,fg(cyan)], "~s~t~40|", [Key]),
    write(" has value "),
    ansi_format([bold,fg(cyan)], "~s~t~40|", [Value]),
    % (
    %     get_assoc(Key, Knowledge, User_Value)
    %     ->
    %     (
    %         % string(User_Value)
    %         % ->
    %         % ansi_format([bold,fg(yellow)], " you answered ~s~t~40|", [User_Value])
    %         % ; 
    %         with_output_to(string(User_Value_Text), portray_clause(User_Value)),
    %         ansi_format([bold,fg(yellow)], " you answered ~s~t~40|", [User_Value_Text])
    %     )
    %     ; true
    % ),
    nl.

describe_winner(Knowledge, (Name, Properties)) :-
    write("The system has matched: "),
    green(Name),
    write(" with score: "),
    compute_optimistic_score(Knowledge, Properties, Score),
    format(string(Score_Text), '~3f', Score),
    green(Score_Text),nl,
    load_assoc_database(Name, Assoc_Values),
    assoc_to_list(Assoc_Values, List),
    maplist(display_product_property(Knowledge), List).

finalize(List_Of_Surviving_Products, Knowledge) :-
    maplist(describe_winner(Knowledge), List_Of_Surviving_Products).

reason(Threshold, Knowledge, List_Of_Surviving_Products) :-
    % TODO ask about most common question among survivers that is still not present
    % aggregate_all(count, member((_, Props), List_Of_Surviving_Products), get_assoc(Key, Props, Value)
    % , Count).
    
    % just ask some question for now if it has not been asked
    findnsols(1, Key,
        (
            member((Name, Properties), List_Of_Surviving_Products),
            gen_assoc(Key, Properties, _),
            not(get_assoc(Key, Knowledge, _))
        ),
    Bag),
    % y(Bag),nl,
    (
        Bag = [Key0]
        ->  (
            ask_question(Key0, Answer),
            % add answer to knowledge
            put_assoc(Key0, Knowledge, Answer, New_Knowledge),
            % recompute questions' score
            % recursively reason about survivors when compared to Threshold
            findall((Name, Properties),
                (
                    member((Name, Properties), List_Of_Surviving_Products),
                    compute_optimistic_score(New_Knowledge, Properties, Score),
                    % cyan(Name),magenta(Score),nl,
                    Score >= Threshold
                ),
                New_Survivors),
            length(New_Survivors, Length),
            nl,write("System matched "),cyan(Length),write(" products"),nl,nl,
            reason(Threshold, New_Knowledge, New_Survivors)
        )
        ; finalize(List_Of_Surviving_Products, Knowledge)
    ).
    % if there is nothing more to ask display the results
    % filter products according to users answer that have chance of survival less than Threshold
    
reason(_, _, []) :-
    ansi_format([bold,fg(red)], "Sorry, no product matches your criteria.\n", []).

user_main :- 
    writeln_fancy('Welcome to diabetics user system'),nl,
    findall((Name, Properties), load_assoc_database(Name, Properties), Bag),
    empty_assoc(Beginning_Knowledge),
    reason(0.7, Beginning_Knowledge, Bag).

:- user_main.




%-----------------------------------------------------------------------------------------------

% ask user to answer questions while there exist questions that could score higher than specified

% float2(F, Head, Tail) :-
%     float(F), !,
%     with_output_to(chars(Head, Tail), write(F)).
% float2(F) -->
%     number(F),
%     { float(F) }.

% flt(F1) --> float2(F1).
% flt(F1) --> {number_codes(F1, F2)}, float2(F2).
% flt(F1) --> {phrase(float(F1), Codes), maplist(char_code, F2, Codes)}, F2.

% extract_valid_answer(Question_Prompt, number, Value_Logic_Tree) :-
%     writeln(Question_Prompt),
%     writeln('Please enter a range of integers e.g. '),
%     writeln('less than 300'),
%     writeln('more than 20'),
%     writeln('between 24 and 98'),
%     current_input(Input),
%     read_string(Input, "\n", "", _, String),
%     normalize_space(string(Input_Normalized), String),
%     string_chars(Input_Normalized, String_Chars),
%     phrase(number_input(Value_Logic_Tree), String_Chars).


% extract_valid_answer(Question_Prompt, subset, Value_Logic_Tree):-.
% extract_valid_answer(Question_Prompt, fuzzy_subset, Value_Logic_Tree):-.

% ask_for_information(Question, Value_Logic_Tree):-
%     nl,
%     question(Question, Question_Prompt, Question_Type),
%     writeln(Question_Prompt),
%     (extract_valid_answer(Question, Question_Type, Value_Logic_Tree) 
%     ; ask_for_information(Question, Value_Logic_Tree)).

% get_answer_from_knowledge_or_ask(Knowledge, Question, Value_Logic_Tree) :-
%     get_assoc(Question, Knowledge, Value_Logic_Tree) -> true
%     ; (ask_for_information(Question, Value_Logic_Tree)).

% :- empty_assoc(Knowledge),
%     get_answer_from_knowledge_or_ask(Knowledge, calories, VLT),
%     write(VLT).

% :- phrase(flt(12.34), F),
%     string_chars(S, F),
%     S = "12.34",
%     writeln(S).

% :-
%     S = "12.34",
%     string_chars(S, F),
%     write(F),
%     phrase(flt(G), F),
%     writeln(G).

% :- string_chars('more than 400', String_Chars),
%     writeln(String_Chars),
%     phrase(number_input(Value_Logic_Tree), String_Chars),
%     writeln(Value_Logic_Tree).

% store all products
% ask a question
% filter out questions that have the potential to survive
% ask another meaningful question (maybe most common to all questions)
% filter out even more
% if no more questions are there to ask or the list of potential products
%   is simply empty, display results



% print_information :-
%     writeln("---------User module---------").

% ask_question(FEATURE, ANSWER) :-
%     question(FEATURE, QUESTION),
%     writeln(QUESTION),
%     read(ANSWER),
%     assertz(result(FEATURE, ANSWER)).

% ask_user(ID) :-
%     findall((FEATURE, VALUE), dish_characteristic(ID, FEATURE, VALUE), LIST),
%     ask_question(FEATURE, VALUE).

% user_main :-
%     print_information,
%     dish_information(ID, _),
%     ask_user(ID).

% %just testing
% %:-
%    user_main.

% :- user_main.