:- module(expert, 
	[  ask_for_new_diagnosis/0
	]).
:- use_module(library(persistency)).
:- persistent database(diagnosis:atom, question_answer_list:list).
:- persistent questions(question_type).
:- db_attach(bazadanych_suplementacja, []).


% :- dynamic database/2.

questions_save([QUESTION | QUESTIONS]) :-
	assert_questions(QUESTION),
	questions_save(QUESTIONS).
questions_save([]).


	

ask_for_new_diagnosis() :-
	nl,
	writeln("Please select the operation you wish to perform:"),
	writeln("(e)dit a diagnosis"),
	writeln("(d)elete a diagnosis"),
	writeln("(a)dd a new diagnosis"),
	writeln("(r)eset the whole database"),
	read(CHAR),
	perform_action(CHAR),
	ask_for_new_diagnosis().

perform_action(a) :- 
	writeln("Please input the diagnosis text:"),
	read(DIAGNOSIS_TEXT),
	add_questions(y, NEW_QUESTIONS, QUESTIONS_WITH_ANSWERS),
	questions_save(NEW_QUESTIONS).
	database_save(DIAGNOSIS_TEXT, QUESTIONS_WITH_ANSWERS).

% TODO
perform_action(d).
perform_action(e).
perform_action(r) :-
	writeln("Please type in the word DELETE to confirm your intentions."),
	read(TEXT),
	TEXT = "DELETE",
	writeln("Deleting database"),
	retractall_questions(_),
	retractall_database(_,_).
% perform_action(_).


add_questions(y, NEW_QUESTIONS_TOTAL, [QUESTION_WITH_ANSWER|QUESTIONS_WITH_ANSWERS]) :- 
	writeln("Would you like to create a new question? Which type? If not, you can try to reuse existing set of questions and provide them with new answer set:"),
	writeln("(r)euse existing question with different answer set"),
	writeln("(u)niversal question with answer set"),
	writeln("(s)single range question with answer set"),
	writeln("(d)ouble range question with answer set"),
	read(CHAR1),
	create_question(CHAR1, NEW_QUESTION, QUESTION_WITH_ANSWER),
	writeln("Would you like to add another question? y/n"),
	read(CHAR_ANOTHER),
	add_questions(CHAR_ANOTHER, NEW_QUESTIONS_RECURSIVE, QUESTIONS_WITH_ANSWERS),
	append([NEW_QUESTION], NEW_QUESTIONS_RECURSIVE, NEW_QUESTIONS_TOTAL).
add_questions(n, [], []).

create_question(u, NEW_QUESTION, QUESTION_WITH_ANSWER) :-
	writeln("Please enter the universal question prompt (to what degree the user possesses some feature):"),
	read(PROMPT),
	not(questions(universal(PROMPT, _, _))),
	writeln("You will be asked to enter answers in decreasing order of possession of the feature."),
	writeln("Please enter the first possible answer (highest degree of possession of the feature):"),
	read(POSSIBLE_ANSWER1),
	writeln("Please enter the second possible answer (lower degree of possession of the feature):"),
	read(POSSIBLE_ANSWER2),
	writeln("Would you like to add further answers to the universal question? y/n"),
	read(CHAR),
	provide_universal_possible_answers(CHAR, POSSIBLE_ANSWERS),
%
	writeln("You will now be asked to enter the correct set of answers in any order."),
	writeln("Please enter the first valid answer:"),
	read(VALID_ANSWER1),
	writeln("Would you like to add further valid answers to the universal question? y/n"),
	read(CHAR),
	provide_universal_valid_answers(CHAR, VALID_ANSWERS),
%
	writeln("Please enter the gaussian sensitivity ratio as a rational nonnegative number."),
	writeln("Zero means for the user that choosing any answer other than the correct one will not score him points."),
	writeln("0.25 represents a reasonable acceptance sensitivity ratio."),
	read(SENSITIVITY),
	append([POSSIBLE_ANSWER1, POSSIBLE_ANSWER2], POSSIBLE_ANSWERS, NEW_QUESTION_ANSWERS),
	NEW_QUESTION = universal(PROMPT, NEW_QUESTION_ANSWERS, SENSITIVITY),
	append([VALID_ANSWER1], VALID_ANSWERS, NEW_VALID_ANSWERS),
	QUESTION_WITH_ANSWER = universal_answered(PROMPT, NEW_VALID_ANSWERS),
	list_to_set(NEW_QUESTION_ANSWERS, NEW_QUESTION_ANSWERS_SET),
	list_to_set(NEW_VALID_ANSWERS, NEW_VALID_ANSWERS_SET),
	subset(NEW_VALID_ANSWERS_SET, NEW_QUESTION_ANSWERS_SET).

provide_universal_possible_answers(y, [POSSIBLE_ANSWER | POSSIBLE_ANSWERS]) :- 
	writeln("Please enter the next possible answer (even lower degree of possession of the feature):"),
	read(POSSIBLE_ANSWER),
	writeln("Would you like to add further answers to the universal question? y/n"),
	read(CHAR),
	provide_universal_possible_answers(CHAR, POSSIBLE_ANSWERS).
provide_universal_possible_answers(n, []).

provide_universal_valid_answers(y, [VALID_ANSWER | VALID_ANSWERS]) :- 
	writeln("Please enter the next valid answer:"),
	read(VALID_ANSWER),
	writeln("Would you like to add further valid answers to the universal question? y/n"),
	read(CHAR),
	provide_universal_valid_answers(CHAR, VALID_ANSWERS).
provide_universal_valid_answers(n, []).


% TODO
create_question(r, NEW_QUESTIONS, NEW_QUESTIONS, QUESTION).
create_question(s, NEW_QUESTIONS, [NEW_QUESTION|NEW_QUESTIONS], QUESTION).
create_question(d, NEW_QUESTIONS, [NEW_QUESTION|NEW_QUESTIONS], QUESTION).



:- ask_for_new_diagnosis.

% TOCLEAN
% databaseReset():-
% 	retractall_database(_,_),
% 	assert_database('You are vitamin A deficient', [yesno('Do you experience night blindness?','yes'), yesno('Is your skin dry?','yes'), yesno('Is bleh?','no')]),
% 	assert_database('You are vitamin C deficient', [yesno('Do you blah?','yes'), yesno('Is bleh?','no')]).

% addBlankAdvisement(Advisement) :-
% 	database(Advisement, _) -> throw('Diagnosis already exists')
% 	;
% 	assert_database(Advisement, []).

% addUniversalQuestion(Advisement, Question, Answers, CorrectSubset) :-
% 	database(Advisement, Questions),
% 	not(member(universal(Question, _, _), Questions)),
% 	retract_database(Advisement, Questions),
% 	assert_database(Advisement, [universal(Question, Answers, CorrectSubset) | Questions]).
