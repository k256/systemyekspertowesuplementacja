% Dziękuję za Wasze przykłady, teraz robimy full-blown database i musiałem pozmieniać

:- dynamic answered/2.

pull_from_database_or_ask(ID, QUESTION, ANSWER) :-
     % if found in database, return answer
     answered(ID, ANSWER);
     % ask only when not found in database
     not(answered(ID, _)),
     format("~w\n", [QUESTION]),
     read(ANSWER),
     assertz(answered(ID, ANSWER)).


ask(negate(QUESTIONS)) :-
     % make sure in its subtree it is pessimisticallyUnsatisfiable
     not(definitelySatisfiable(QUESTIONS)),
     (not(possiblySatisfiable(QUESTIONS)); not(ask(QUESTIONS))).

ask(or(QUESTION1, QUESTION2)) :-
     % we must have at least a chance to win
     (possiblySatisfiable(QUESTION1) ; possiblySatisfiable(QUESTION2)),
     % skip subtrees and their neighbouring-disjunctive subtrees that are known to be satisfiable in worst case
     (
          definitelySatisfiable(QUESTION1) ; definitelySatisfiable(QUESTION2) ; ask(QUESTION1) ; definitelySatisfiable(QUESTION2) ;ask(QUESTION2)
     ).

ask(and(QUESTION1, QUESTION2)) :-
     % we must have at least a chance to win
     possiblySatisfiable(QUESTION1) , possiblySatisfiable(QUESTION2),
     (
          % skip subtrees that are known to be satisfiable in worst case
          (definitelySatisfiable(QUESTION1) ; ask(QUESTION1)),
          % skip subtrees that are known to be satisfiable in worst case
          (definitelySatisfiable(QUESTION2) ; ask(QUESTION2))
     ).

ask(implies(QUESTION1, QUESTION2)) :-
     % we must have at least a chance to win
     
     (not(definitelySatisfiable(QUESTION1)); possiblySatisfiable(QUESTION1) , possiblySatisfiable(QUESTION2)),
     (
          % skip if definitely unsatisfiable or simply ask the user if it is unsatisfiable
          (not(possiblySatisfiable(QUESTION1)) ; not(ask(QUESTION1)));
          % skip subtrees that are known to be satisfiable in worst case
          (definitelySatisfiable(QUESTION1) ; ask(QUESTION1)),
          % skip subtrees that are known to be satisfiable in worst case
          (definitelySatisfiable(QUESTION2) ; ask(QUESTION2))
     ).

ask(iff(QUESTION1, QUESTION2)) :-
     % we must have at least a chance to win
     
     (not(definitelySatisfiable(QUESTION1)), not(definitelySatisfiable(QUESTION2)) ; possiblySatisfiable(QUESTION1) , possiblySatisfiable(QUESTION2)),
     (
          % skip if definitely unsatisfiable or simply ask the user if it is unsatisfiable
          (not(possiblySatisfiable(QUESTION1)) ; not(ask(QUESTION1))), (not(possiblySatisfiable(QUESTION2)) ; not(ask(QUESTION2))) ;
          % skip subtrees that are known to be satisfiable in worst case
          (definitelySatisfiable(QUESTION1) ; ask(QUESTION1)),
          % skip subtrees that are known to be satisfiable in worst case
          (definitelySatisfiable(QUESTION2) ; ask(QUESTION2))
     ).

ask(yesno(PROMPT, CORRECT_ANSWER)) :-
     atom_concat(PROMPT, ' please answer yes. or no.', EXTENDED_PROMPT),
     pull_from_database_or_ask(PROMPT, EXTENDED_PROMPT, ANSWER),
     ANSWER = CORRECT_ANSWER.

ask(value(PROMPT, RELATION, BOUNDARY)) :-
     pull_from_database_or_ask(PROMPT, PROMPT, ANSWER),
     withinBoundary(ANSWER, BOUNDARY, RELATION).



possiblySatisfiable(or(QUESTION1, QUESTION2)) :-
     possiblySatisfiable(QUESTION1) ; possiblySatisfiable(QUESTION2).

possiblySatisfiable(and(QUESTION1, QUESTION2)) :-
     possiblySatisfiable(QUESTION1) , possiblySatisfiable(QUESTION2).

possiblySatisfiable(implies(QUESTION1, QUESTION2)) :-
     not(definitelySatisfiable(QUESTION1)) ; possiblySatisfiable(QUESTION1) , possiblySatisfiable(QUESTION2).

possiblySatisfiable(iff(QUESTION1, QUESTION2)) :-
     not(definitelySatisfiable(QUESTION1)) , not(definitelySatisfiable(QUESTION2)) ; possiblySatisfiable(QUESTION1) , possiblySatisfiable(QUESTION2).

possiblySatisfiable(yesno(PROMPT, CORRECT_ANSWER)) :-
     trueorunassignedYESNO(PROMPT, CORRECT_ANSWER).

possiblySatisfiable(value(PROMPT, RELATION, BOUNDARY)) :-
     trueorunassignedVALUE(PROMPT, RELATION, BOUNDARY).

possiblySatisfiable(negate(QUESTIONS)) :-
     not(definitelySatisfiable(QUESTIONS)).

trueorunassignedYESNO(PROMPT, CORRECT_ANSWER) :-
     not(answered(PROMPT, _));
     answered(PROMPT, ANSWER), ANSWER=CORRECT_ANSWER.

trueorunassignedVALUE(PROMPT, RELATION, BOUNDARY) :-
     not(answered(PROMPT, _));
     (answered(PROMPT, ANSWER), withinBoundary(ANSWER, BOUNDARY, RELATION)).



definitelySatisfiable(or(QUESTION1, QUESTION2)) :-
     definitelySatisfiable(QUESTION1) ; definitelySatisfiable(QUESTION2).

definitelySatisfiable(and(QUESTION1, QUESTION2)) :-
     definitelySatisfiable(QUESTION1) , definitelySatisfiable(QUESTION2).

definitelySatisfiable(implies(QUESTION1, QUESTION2)) :-
     not(possiblySatisfiable(QUESTION1)) ; definitelySatisfiable(QUESTION1) , definitelySatisfiable(QUESTION2).

definitelySatisfiable(iff(QUESTION1, QUESTION2)) :-
     not(possiblySatisfiable(QUESTION1)) , not(possiblySatisfiable(QUESTION2)) ; definitelySatisfiable(QUESTION1) , definitelySatisfiable(QUESTION2).

definitelySatisfiable(yesno(PROMPT, CORRECT_ANSWER)) :-
     trueYESNO(PROMPT, CORRECT_ANSWER).

definitelySatisfiable(value(PROMPT, RELATION, BOUNDARY)) :-
     trueVALUE(PROMPT, RELATION, BOUNDARY).

definitelySatisfiable(negate(QUESTIONS)) :-
     not(possiblySatisfiable(QUESTIONS)).

trueYESNO(PROMPT, CORRECT_ANSWER) :-
     answered(PROMPT, ANSWER), ANSWER=CORRECT_ANSWER.

trueVALUE(PROMPT, RELATION, BOUNDARY) :-
     answered(PROMPT, ANSWER), withinBoundary(ANSWER, BOUNDARY, RELATION).

withinBoundary(ANSWER, BOUNDARY, 'gt') :- ANSWER > BOUNDARY.
withinBoundary(ANSWER, BOUNDARY, 'lt') :- ANSWER =< BOUNDARY.



askfromdatabase() :-
     foreach(
          (database(ADVICE, STRUCTURE, QUESTIONS), filledtree(STRUCTURE, QUESTIONS, TREEOUT)),
          (
               (   % already could be determined that it's true
                    possiblySatisfiable(TREEOUT),
                    % make sure the subtree is satisfiable
                    (definitelySatisfiable(TREEOUT); ask(TREEOUT))
               )
                 -> 
                    format("~w\n", ADVICE),
                    (
                         write('Do you wish to receive further advice? please answer yes. or no.'), nl, read(ANSWER), ANSWER='yes'
                         ; write('Thank you.'), nl, fail
                    )
          )
     ).

advise() :-
     consult(database),
     consult(logic_formula_parser),
     ignore(askfromdatabase()),
     % forget what user said since the process is finished
     retractall(answered(_,_)).

:- initialization(advise()).
