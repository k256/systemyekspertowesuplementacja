% :- table logictree//3.
logictree(T, QUESTIONS, LEFTOVER) --> logictreeor(T, QUESTIONS, LEFTOVER).
logictreeor(or(A,B), QUESTIONS, LEFTOVER) --> logictreeand(A, QUESTIONS, LEFTOVER1),['|'],logictreeor(B, LEFTOVER1, LEFTOVER).
logictreeor(A, QUESTIONS, LEFTOVER) --> logictreeand(A, QUESTIONS, LEFTOVER).
logictreeand(and(A,B), QUESTIONS, LEFTOVER) --> logictreeimplies(A, QUESTIONS, LEFTOVER1),['&'],logictreeand(B, LEFTOVER1, LEFTOVER).
logictreeand(A, QUESTIONS, LEFTOVER) --> logictreeimplies(A, QUESTIONS, LEFTOVER).
logictreeimplies(implies(A,B), QUESTIONS, LEFTOVER) --> logictreeiff(A, QUESTIONS, LEFTOVER1),['='],['>'],logictreeimplies(B, LEFTOVER1, LEFTOVER).
logictreeimplies(A, QUESTIONS, LEFTOVER) --> logictreeiff(A, QUESTIONS, LEFTOVER).
logictreeiff(iff(A,B), QUESTIONS, LEFTOVER) --> logictreeterm(A, QUESTIONS, LEFTOVER1),['<'],['='],['>'],logictreeiff(B, LEFTOVER1, LEFTOVER).
logictreeiff(A, QUESTIONS, LEFTOVER) --> logictreeterm(A, QUESTIONS, LEFTOVER).
logictreeterm(negate(T), QUESTIONS, LEFTOVER) --> ['!'],logictree(T, QUESTIONS, LEFTOVER).
logictreeterm(T, QUESTIONS, LEFTOVER) --> ['('],logictree(T, QUESTIONS, LEFTOVER),[')'].
logictreeterm(QUESTION, [QUESTION|LEFTOVER], LEFTOVER) --> [q].

filledtree(STRUCTURE, QUESTIONS, TREE) :-
    string_chars(STRUCTURE, CHARS),
    delete(CHARS, ' ', CHARSNOWHITESPACE),    
    phrase(logictree(TREE, QUESTIONS, []), CHARSNOWHITESPACE, []).