:- module(database, [
    load_assoc_database/2,
    assert_to_database/2,
    delete_from_database/1
]).

:- use_module(library(persistency)).
:- persistent database(name:string, diagnosis_list:list).
:- db_attach(bazadanych_suplementacja, []).

load_assoc_database(Name, Assoc_Values) :-
    database(Name, List_Of_Values),
    list_to_assoc(List_Of_Values, Assoc_Values).

assert_to_database(Name, Assoc_Values) :-
    % map_assoc(database_entry_assoc_to_list, Assoc_Values, Assoc_Of_Lists),
%    delete_from_database(Name),
    assoc_to_list(Assoc_Values, List),
    assert_database(Name, List),
    db_sync(gc).
    % writeln('written').

    %todo delete from database
delete_from_database(Name) :-
    retract_database(Name, _).
