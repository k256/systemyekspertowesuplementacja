:- module(rules, [
   question/3,
   valid_subset_answer/2,
   valid_fuzzy_subset_answer/3
]).

%characteristics
%characteristic(calories, "Calories").

%characteristics properties
%characteristics_type(calories, number, [0, 100]).

%questions

%cechy 
%sugar, mass, vitamins, fat, digestiveness, restriction, category, containts_lactose, hydration_level

%dairy??

question(calories, "How many calories do you want?", number).
question(sugar, "How much sugar do you want?", number).
question(vitamins, "What vitamins do you want?", subset).
question(fat, "How much fat do you want?", number).
question(digestiveness, "What kind of meal do you want for your stomache?", subset).
question(restriction, "What kind of diet are you on?", subset).
question(category, "What category of meal do you want?", subset).
question(contains_lactose, "Do you want your product to contain lactose?", fuzzy_subset).
question(hydration_level, "How hydrated you want your meal to be?", fuzzy_subset).
valid_subset_answer(vitamins, "A").
valid_subset_answer(vitamins, "B").
valid_subset_answer(vitamins, "B6").
valid_subset_answer(vitamins, "C").
valid_subset_answer(vitamins, "D").
valid_subset_answer(digestiveness, easy).
valid_subset_answer(digestiveness, medium).
valid_subset_answer(digestiveness, difficult).
valid_subset_answer(restriction, vegan).
valid_subset_answer(restriction, vegetarian).
valid_subset_answer(restriction, frutarian).
valid_subset_answer(category, vegetable).
valid_subset_answer(category, dish).
valid_subset_answer(category, meat).
valid_subset_answer(category, fruit).
valid_fuzzy_subset_answer(contains_lactose, "low", 0.2).
valid_fuzzy_subset_answer(contains_lactose, "medium", 0.5).
valid_fuzzy_subset_answer(contains_lactose, "high", 0.9).
valid_fuzzy_subset_answer(hydration_level, "dry", 0.1).
valid_fuzzy_subset_answer(hydration_level, "medium", 0.35).
valid_fuzzy_subset_answer(hydration_level, "wet", 0.85).
