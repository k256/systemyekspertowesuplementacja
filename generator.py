import random

def readDishes():
    dishes = []
    with open('data/dishes.txt', 'r') as f:
        for line in f.readlines():
            dishes.append(line.strip())
    return dishes

def readCalories():
    dishes = []
    with open('data/calories.txt', 'r') as f:
        for line in f.readlines():
            dishes.append(line.replace(' ','').strip())
    return dishes

def readCategory():
    dishes = []
    with open('data/category.txt', 'r') as f:
        for line in f.readlines():
            dishes.append(line.replace(' ','').strip())
    return dishes

def readDigist():
    dishes = []
    with open('data/digistivness.txt', 'r') as f:
        for line in f.readlines():
            dishes.append(line.replace(' ','').strip())
    return dishes

def readFat():
    dishes = []
    with open('data/fat.txt', 'r') as f:
        for line in f.readlines():
            dishes.append(line.replace(' ','').strip())
    return dishes

def readLactose():
    dishes = []
    with open('data/lactose.txt', 'r') as f:
        for line in f.readlines():
            dishes.append(line.replace(' ','').strip())
    return dishes

def readRestriction():
    dishes = []
    with open('data/restrictions.txt', 'r') as f:
        for line in f.readlines():
            dishes.append(line.replace(' ','').strip())
    return dishes

def readSugar():
    dishes = []
    with open('data/sugar.txt', 'r') as f:
        for line in f.readlines():
            dishes.append(line.replace(' ','').strip())
    return dishes

def readVitamin():
    dishes = []
    with open('data/vitamins.txt', 'r') as f:
        for line in f.readlines():
            dishes.append(line.replace(' ','').strip())
    return dishes

def writeToFile():
    dishes = readDishes()
    calories = readCalories()
    category = readCategory()
    dis = readDigist()
    fat = readFat()
    lactose = readLactose()
    restriction = readRestriction()
    sugar = readSugar()
    vitamin = readVitamin()
    with open('bazadanych_suplementacja', 'w') as f:

        numDishes = len(dishes)
        f.write("created(1558639395.6128082).\n")
        for i in range(numDishes):
            line = "assert(database(\""+str(dishes[i])+"\",["

            if(calories[i] != ""):
                line = line + "calories-\""+str(calories[i]) + "\","
            if(category[i] != ""):
                line = line + "category-\""+str(category[i]) + "\","
            if(dis[i] != ""):
                line = line + "digestiveness-\""+str(dis[i]) + "\","                                
            if(fat[i] != ""):
                line = line + "fat-\""+str(fat[i]) + "\","
            if(lactose[i] != ""):
                line = line + "containts_lactose-\""+str(lactose[i]) + "\","
            if(restriction[i] != ""):
                line = line + "restriction-\""+str(restriction[i]) + "\","
            if(sugar[i] != ""):
                line = line + "sugar-\""+str(sugar[i]) + "\","
            if(vitamin[i] != ""):
                line = line + "vitamins-\""+str(vitamin[i]) + "\","
            if(random.random() > 0.25):
                line = line + "hydration_level-\""+random.choice(["dry", "medium", "wet"]) + "\","

            line = line[:-1] + "])).\n"
            f.write(line)

if __name__ == '__main__':
    writeToFile()
